#include <limits>
#include "gtest/gtest.h"
#include "polynomial.h"

TEST(EqualsOperator, TwoEqualPolynomials) {
    const int ITER_COUNT = 9;
    Polynomial<int> left(1);
    Polynomial<int> right(1);

    for (int i = 1; i < ITER_COUNT; ++i) {
        right[i] = left[i] = i + 1;
    }

    EXPECT_EQ(left, right);
}


TEST(EqualsOperator, TwoNonEqualPolynomials) {
    const int ITER_COUNT = 9;
    Polynomial<int> left(1);
    Polynomial<int> right(1);

    for (int i = 1; i < ITER_COUNT; ++i) {
        right[i] = left[i] = i + 1;
    }
    right[15] = 16;

    EXPECT_NE(left, right);
}

TEST(EqualsOperator, LastCoefficientTest) {
    Polynomial<int> a({1, 2, 3, 4});
    Polynomial<int> b({1, 2, 3, 5});
    EXPECT_NE(a, b);
}


TEST(EqualsOperator, ThreePolynomials) {
    const int ITER_COUNT = 50;
    Polynomial<int> left(1);
    Polynomial<int> right(1);

    for (int i = 1; i < ITER_COUNT; ++i) {
        right[i] = left[i] = i + 1;
    }

    Polynomial<int> middle = right;
    EXPECT_EQ(left, middle);

    right[ITER_COUNT - 1] = 0;
    EXPECT_NE(middle, right);
}

TEST(Degree, ZeroPolynomial) {
    Polynomial<int> polynomial(0);
    EXPECT_EQ(polynomial.degree(), -1);
}

TEST(Constructor, InitializerList) {
    Polynomial<int> polynomial({});
    EXPECT_EQ(Polynomial<int>(), polynomial);
}

TEST(Degree, NonZeroPolynomial) {
    const int ITER_COUNT = 50;
    Polynomial<int> polynomial(0);
    for (int i = 0; i < ITER_COUNT; ++i) {
        polynomial[i] = i;
    }
    EXPECT_EQ(polynomial.degree(), ITER_COUNT - 1);
}

TEST(Degree, DegreeChanges) {
    Polynomial<int> polynomial(0);
    polynomial[100] = 1;
    polynomial[50] = 1;
    EXPECT_EQ(polynomial.degree(), 100);

    polynomial[100] = 0;
    EXPECT_EQ(polynomial.degree(), 50);
}

TEST(Degree, BigPolynomial) {
    const int DEGREE = std::numeric_limits<int>::max() / 1024;
    Polynomial<int> polynomial(0);
    polynomial[DEGREE] = 1;
    EXPECT_EQ(polynomial.degree(), DEGREE);
}

TEST(ConstReferences, Polynomial) {
    Polynomial<double> a(3);
    const auto& b = a;
    const auto& c5 = b[1024];
    a[1024] = 42.0;
    EXPECT_EQ(a[1024], c5);
}

TEST(OperationsWithScalar, ScalarRight) {
    Polynomial<double> a({1, 2, 1});
    Polynomial<double> c(0);
    c = a + 1;
    EXPECT_EQ(Polynomial<double> ({2, 2, 1}), c);
    c = a - 2;
    EXPECT_EQ(Polynomial<double> ({-1, 2, 1}), c);
    c = a * 2;
    EXPECT_EQ(Polynomial<double> ({2, 4, 2}), c);
    c = c / 2;
    EXPECT_EQ(a, c);
    c = c % a;
    EXPECT_EQ(.0, c);
}

TEST(OperationsWithScalar, ScalarLeft) {
    Polynomial<double> a({1, 2, 1});
    Polynomial<double> c(0);
    c = 1. + a;
    EXPECT_EQ(Polynomial<double> ({2, 2, 1}), c);
    c = -2. + a;
    EXPECT_EQ(Polynomial<double> ({-1, 2, 1}), c);
    c = 2. * a;
    EXPECT_EQ(Polynomial<double> ({2, 4, 2}), c);
}

TEST(OperationsWithScalar, OperatorEqual) {
    Polynomial<double> a({1, 2, 1});
    a += 1;
    EXPECT_EQ(Polynomial<double> ({2, 2, 1}), a);
    a -= 2;
    EXPECT_EQ(Polynomial<double> ({0, 2, 1}), a);
    a *= 2;
    EXPECT_EQ(Polynomial<double> ({0, 4, 2}), a);
    a /= 2;
    EXPECT_EQ(Polynomial<double> ({0, 2, 1}), a);
    a %= 2;
    EXPECT_EQ(.0, a);
}

TEST(Sum, TwoSmallPolynomials) {
    Polynomial<int> left(0);
    Polynomial<int> right(0);
    left[5] = 1;
    right[5] = 1;
    right[7] = 2;

    Polynomial<int> expected(0);
    expected[5] = 2;
    expected[7] = 2;

    EXPECT_EQ(expected, left + right);
}

TEST(Sum, CommutativeProperty) {
    Polynomial<int> first(0);
    Polynomial<int> second(0);
    first[5] = 1;
    second[6] = 1;
    EXPECT_EQ(first + second, second + first);
}

TEST(Sum, AssociativeProperty) {
    Polynomial<int> first(0);
    Polynomial<int> second(0);
    Polynomial<int> third(0);

    first[5] = 1;
    second[6] = 1;
    third[7] = 1;

    EXPECT_EQ((first + second) + third, first + (second + third));
}

TEST(Difference, TwoPolynomials) {
    Polynomial<int> first(0);
    Polynomial<int> second(0);
    first[5] = 1;
    second[6] = 1;
    Polynomial<int> third(first + second);
    EXPECT_EQ(first, third - second);
}

TEST(UnaryMinus, SmallPolynomial) {
    const int DEGREE = 100;
    Polynomial<int> polynomial(10);
    for (int i = 1; i <= DEGREE; ++i) {
        polynomial[i] = i;
    }
    Polynomial<int> expected(-10);
    for (int i = 1; i <= DEGREE; ++i) {
        expected[i] = -i;
    }
    EXPECT_EQ(expected, -polynomial);
}

TEST(Multiplication, PolynomialSquare) {
    Polynomial<int> left({1, 1}); // x + 1
    Polynomial<int> expected({1, 2, 1}); // x^2 + 2*x + 1
    EXPECT_EQ(expected, left * left);
}

TEST(Multiplication, PolynomialCube) {
    Polynomial<int> left({-1, 1}); // x - 1
    Polynomial<int> expected({-1, 3, -3, 1}); // x^3 - 3x^2 + 3x - 1
    EXPECT_EQ(expected, left * left * left);
}

TEST(Multiplication, CommutativeProperty) {
    Polynomial<int> left({-1, 1}); // x - 1
    Polynomial<int> right({1, 1});
    EXPECT_EQ(left * right, right * left);
}

TEST(Multiplication, AssociativeProperty) {
    Polynomial<int> first(0); // x - 1
    first[3] = 3;

    Polynomial<int> second(0);
    second[5] = 5;

    Polynomial<int> third(0);
    third[7] = 7;

    Polynomial<int> expected(0);
    expected[3 + 5 + 7] = 3 * 5 * 7;

    EXPECT_EQ(expected, first * (second * third));
    EXPECT_EQ((first * second) * third, first * (second * third));
}

TEST(SumAndMultiplication, DistributiveProperty) {
    Polynomial<int> first({-1, 1}); // x - 1
    Polynomial<int> second({1, 1}); // x + 1
    Polynomial<int> third({1, 1, 1}); // x^2 + x + 1
    Polynomial<int> expected({-2, 0, 1, 1});
    EXPECT_EQ(expected, first * (second + third));
    EXPECT_EQ(first * second + first * third, first * (second + third));
}

TEST(PlusEqual, AddNonZeroPolynomial) {
    Polynomial<int> first({-1, 1}); // x - 1
    Polynomial<int> second({1, 1, 1, 2}); // 2x^3 + x^2 + x + 1
    Polynomial<int> expected({0, 2, 1, 2});
    first += second;
    EXPECT_EQ(expected, first);
}

TEST(PlusEqual, AddExpression) {
    Polynomial<int> first({-1, 1}); // x - 1
    Polynomial<int> second({1, 1, 1, 2}); // 2x^3 + x^2 + x + 1
    Polynomial<int> third(0); // 0
    EXPECT_EQ(first, first += (second * third - first * third));
}

TEST(MinusEqual, MinusExpression) {
    Polynomial<int> first({-1, 1}); // x - 1
    Polynomial<int> second({1, 1, 1, 2}); // 2x^3 + x^2 + x + 1
    Polynomial<int> expected(0);
    expected[3] = 2;
    expected[1] = 3;
    EXPECT_EQ(expected, second -= first * first);
}

TEST(OperatorParenthesis, ZeroPolynomial) {
    Polynomial<double> polynomial(0);
    EXPECT_EQ(0., polynomial(10));
}

TEST(OperatorParenthesis, NonZeroPolynomial) {
    Polynomial<double> polynomial({1, 2, 1});
    EXPECT_EQ(4, polynomial(1));
}

TEST(DivisionAndRemainder, NonZeroPolynomial) {
    Polynomial<int> dividend({2, 2, 1});
    Polynomial<int> divisor({1, 1});
    Polynomial<int> rest(1);
    Polynomial<int> result = dividend / divisor;
    EXPECT_EQ(divisor, dividend / divisor);
    EXPECT_EQ(rest, dividend % divisor);
}

TEST(DivisionAndRemainder, SimpleTest) {
    Polynomial<int> dividend({2, 2, 1});
    Polynomial<int> divisor(-1);
    Polynomial<int> rest(0);

    EXPECT_EQ(dividend * divisor, dividend / divisor);
    EXPECT_EQ(rest, dividend % divisor);
}

TEST(DivisionAndRemainder, DivideByGreaterPolynomial) {
    Polynomial<int> dividend({2, 2, 1});
    Polynomial<int> divisor(0);
    divisor[3] = 1;

    Polynomial<int> quotient(0);

    EXPECT_EQ(quotient, dividend / divisor);
    EXPECT_EQ(dividend, dividend % divisor);
}

TEST(DivideEqual, SimpleTest) {
    Polynomial<int> dividend({1, 2, 1});
    Polynomial<int> divisor({1, 1});
    Polynomial<int> quotient(divisor);
    dividend /= divisor;
    EXPECT_EQ(quotient, dividend);
}

TEST(ModuloEqual, SimpleTest) {
    Polynomial<int> dividend({2, 2, 1});
    Polynomial<int> divisor({1, 1});
    Polynomial<int> remainder(1);
    dividend %= divisor;
    EXPECT_EQ(remainder, dividend);
}

TEST(Iterator, SimpleTest) {
    Polynomial<int> polynomial(0);
    polynomial[10] = 10;
    polynomial[20] = 0;

    int i = 0;
    Polynomial<int>::iterator iterator = polynomial.begin();
    for ( ; iterator != polynomial.end(); ++iterator) {
        *iterator = i;
        i++;
    }

    Polynomial<int>::const_iterator const_iterator = polynomial.begin();

    i = 0;
    while (iterator != polynomial.end()) {
        EXPECT_EQ(polynomial[i], *const_iterator);
        i++;
        const_iterator++;
    }
}

TEST(ConstIterator, Iterate) {
    Polynomial<int> a = {1, 1, 0, 0, 0, 0};
    int i = 0;
    for (const auto& coefficient : a) {
        EXPECT_EQ(1, coefficient);
        ++i;
    }
    EXPECT_EQ(2, i);
}

TEST(Iterator, IterateThroughEmptyPolynomial) {
    Polynomial<int> polynomial;
    Polynomial<int>::iterator it = polynomial.begin();
    int iter_count = 0;
    for( ; it != polynomial.end(); ++it, ++iter_count) {
        EXPECT_EQ(0, *it);
    }
    EXPECT_EQ(1, iter_count);
}

TEST(GCD, SimpleTest) {
    Polynomial<int> left({1, 3, 3, 1});
    Polynomial<int> right({1, 1});
    Polynomial<int> gcd = (right, left);
    EXPECT_EQ(right, gcd);
}


TEST(GCD, AnotherSimpleTest) {
    Polynomial<double> left({4, 9, 6, 1});
    Polynomial<double> right({-4, -1, 4, 1});
    Polynomial<double> gcd = (right, left);
    EXPECT_EQ(Polynomial<double>({4, 5, 1, 0}), gcd);
}

TEST(ConstantMinusPolynomial, SimpleTest) {
    int scalar = 6;
    Polynomial<int> polynomial({1,2,3,4,5});
    Polynomial<int> expected({5, -2,-3,-4,-5});
    EXPECT_EQ(expected, scalar - polynomial);
}

TEST(MoveAssignmentOperator, SelfMove) {
    Polynomial<double> a = {1, 1};
    a = std::move(a);
    //expected
    EXPECT_EQ(Polynomial<double>({1, 1}), a);
}
