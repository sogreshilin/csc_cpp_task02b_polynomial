# Polynomial

In this project template class Polynomial implemented as a one-variable polynomial. 
Template parameter is type of polynomial coefficients. 
There are different operations on polynomials available. Overloaded operators: 
- ```==, !=```
- ```+, -, *```
- ```/, %```
- ```gcd```
- ```operator[]``` to get the specific coefficient
- ```begin()```, ```end()``` to get the coefficients iterator
- ```operator()``` that counts the value of polynomial at certain point
- ```operator<<``` that prints polynomial in format ``` 5x^2 + x + 3 ```

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need to have ```cmake``` utility to build a project.

### Installing

A step by step series of examples that tell you how to get a development env running.

Create a build directory:

```
mkdir build/
cd build/
```

Create Makefile:

```
cmake ..
```

Build project and tests:

```
make
```

## Running the tests

To run the automated tests for this system use command:

```
./tests
```

## Running the program

To run the program use command:

```
./polynomial
```