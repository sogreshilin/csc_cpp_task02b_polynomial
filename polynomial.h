//
// Created by Alexandr Sogreshilin on 23/10/2017.
//

#ifndef POLINOM_POLYNOM_H
#define POLINOM_POLYNOM_H


#include <iosfwd>
#include <iostream>
#include <deque>

template <typename T>
class Polynomial final {
public:
    Polynomial() : coefficients(std::deque<T>({0})) {
    }

    Polynomial(const T coefficient)
        : coefficients(std::deque<T>({coefficient})) {
    }

    Polynomial(const Polynomial& other) :
        coefficients(other.coefficients) {
    }

    Polynomial(Polynomial&& other)
        : coefficients(std::move(other.coefficients)) {
        other.coefficients = {0};
    }

    Polynomial(std::initializer_list<T> args)
        : coefficients(args) {
        if (args.size() == 0) {
            coefficients.push_back(0);
        }
    }

    Polynomial<T>& operator=(const Polynomial& other) {
        if (this != &other) {
            coefficients = other.coefficients;
        }
        return *this;
    }

    Polynomial<T>& operator=(Polynomial&& other) {
        if (this != &other) {
            coefficients = std::move(other.coefficients);
            other.coefficients = {0};
        }
        return *this;
    }

    bool operator==(const Polynomial<T>& other) const {
        if (degree() != other.degree()) {
            return false;
        }
        const int degree = this->degree();
        for (int i = 0; i <= degree; ++i) {
            if ((*this)[i] != other[i]) {
                return false;
            }
        }
        return true;
    }

    bool operator==(const T scalar) const {
        if (degree() > 1) {
            return false;
        }
        return coefficients[0] == scalar;
    }

    bool operator!=(const T scalar) const {
        return !(*this == scalar);
    }

    bool operator!=(const Polynomial<T>& other) const {
        return !(*this == other);
    }

    Polynomial<T> operator+(const Polynomial<T>& other) const {
        Polynomial<T> result(other);
        const int degree = std::max(this->degree(), other.degree());
        for (int i = 0; i <= degree; ++i) {
            result[i] = (*this)[i] + other[i];
        }
        return result;
    }

    Polynomial<T> operator+(const T scalar) const {
        Polynomial<T> result(*this);
        result.coefficients[0] += scalar;
        return result;
    }

    Polynomial<T> operator-() const {
        Polynomial<T> result(0);
        const int degree = this->degree();
        for (int i = 0; i <= degree; ++i) {
            result[i] = - (*this)[i];
        }
        return result;
    }

    Polynomial<T> operator-(const Polynomial<T>& other) const {
        return *this + (-other);
    }

    Polynomial<T> operator-(const T scalar) const {
        Polynomial<T> result(*this);
        result.coefficients[0] -= scalar;
        return result;
    }

    Polynomial<T> operator*(const Polynomial<T>& other) const {
        Polynomial<T> result(0);
        const int this_degrree = degree();
        const int other_degree = other.degree();
        for (int i = 0; i <= this_degrree; ++i) {
            for (int j = 0; j <= other_degree; ++j) {
                result[i + j] += (*this)[i] * other[j];
            }
        }
        return result;
    }

    Polynomial<T> operator*(const T scalar) const {
        Polynomial<T> result(0);
        const int degree = this->degree();
        for (int i = 0; i <= degree; ++i) {
            result[i] = (*this)[i] * scalar;
        }
        return result;
    }

    Polynomial<T>& operator+=(const Polynomial<T>& other) {
        const int degree = std::max(this->degree(), other.degree());
        for (int i = 0; i <= degree; ++i) {
            (*this)[i] += other[i];
        }
        return *this;
    }

    Polynomial<T>& operator+=(const T scalar) {
        coefficients[0] += scalar;
        return *this;
    }

    Polynomial<T>& operator-=(const Polynomial<T>& other) {
        const int degree = std::max(this->degree(), other.degree());
        for (int i = 0; i <= degree; ++i) {
            (*this)[i] -= other[i];
        }
        return *this;
    }

    Polynomial<T>& operator-=(const T scalar) {
        coefficients[0] -= scalar;
        return *this;
    }

    Polynomial<T>& operator*=(const Polynomial<T>& other) const {
        // this polynomial in memory has to be in memory
        Polynomial<T> left(*this);
        const int this_degrree = degree();
        const int other_degree = other.degree();
        for (int i = 0; i <= this_degrree; ++i) {
            for (int j = 0; j <= other_degree; ++j) {
                (*this)[i + j] += left[i] * other[j];
            }
        }
        return *this;
    }

    Polynomial<T>& operator*=(const T scalar) {
        for (auto& coefficient : coefficients) {
            coefficient *= scalar;
        }
        return *this;
    }

    int degree() const {
        int degree = -1;
        for (int i = 0; i < coefficients.size(); ++i) {
            if (coefficients[i] != 0) {
                degree = i;
            }
        }
        return degree;
    }

    T& operator[](int index) {
        if (index < 0) {
            throw std::out_of_range("Index out of bound");
        }
        if (index < coefficients.size()) {
            return coefficients[index];
        }
        if (index + 1 < 0) {
            throw std::out_of_range("Polynomial is too big");
        }
        coefficients.resize((index + 1), 0);
        return coefficients[index];
    }

    const T& operator[](int index) const {
        if (index < 0) {
            throw std::out_of_range("Index out of bound");
        }
        if (index < coefficients.size()) {
            return coefficients[index];
        }
        if (index + 1 < 0) {
            throw std::out_of_range("Polynomial is too big");
        }
        coefficients.resize((index + 1), 0);
        return coefficients[index];
    }

    T operator()(T point) const {
        T result = 0;
        T x = 1;
        const int degree = this->degree();
        for (int i = 0; i <= degree; ++i) {
            result += coefficients[i] * x;
            x *= point;
        }
        return result;
    }

    Polynomial<T> operator/(const Polynomial<T>& divisor) const {
        if (divisor.degree() < 0) {
            throw std::invalid_argument("Dividing by zero");
        }
        Polynomial<T> result(0);
        Polynomial<T> remainder = *this;
        while(remainder.degree() >= divisor.degree()) {
            int index = remainder.degree() - divisor.degree();
            result[index] = remainder.coefficients[remainder.degree()]
                / divisor.coefficients[divisor.degree()];
            Polynomial<T> mult(0);
            mult[index] = result[index];
            remainder -= divisor * mult;
        }
        return result;
    }

    Polynomial<T> operator/(const T scalar) const {
        if (scalar == 0) {
            throw std::invalid_argument("Dividing by zero");
        }
        Polynomial<T> result(0);
        const int degree = this->degree();
        for (int i = 0; i <= degree; ++i) {
            result[i] = (*this)[i] / scalar;
        }
        return result;
    }

    Polynomial<T> operator%(const Polynomial<T>& divisor) const {
        if (divisor.degree() < 0) {
            throw std::invalid_argument("Dividing by zero");
        }
        Polynomial<T> result(0);
        Polynomial<T> remainder = *this;
        while(remainder.degree() >= divisor.degree()) {
            int index = remainder.degree() - divisor.degree();
            result[index] = remainder.coefficients[remainder.degree()] /
                divisor.coefficients[divisor.degree()];
            Polynomial<T> mult(0);
            mult[index] = result[index];
            remainder -= divisor * mult;
        }
        return remainder;
    }

    Polynomial<T> operator%(const T scalar) const {
        if (scalar == 0) {
            throw std::invalid_argument("Dividing by zero");
        }
        Polynomial<T> zero_polynomial(0);
        return zero_polynomial(0);
    }

    Polynomial<T>& operator/=(const Polynomial<T>& other) {
        *this = *this / other;
        return *this;
    }

    Polynomial<T>& operator/=(const T scalar) {
        for (auto& coefficient : coefficients) {
            coefficient /= scalar;
        }
        return *this;
    }

    Polynomial<T>& operator%=(const Polynomial<T>& other) {
        *this = *this % other;
        return *this;
    }

    Polynomial<T>& operator%=(const T scalar) {
        return *this = Polynomial<T>(0);
    }

    Polynomial<T>& normalize() {
        if (degree() >= 0) {
            const T scalar = coefficients[degree()];
            *this /= scalar;
        }
        return *this;
    }

    typedef typename std::deque<T>::iterator iterator;

    iterator begin() {
        return coefficients.begin();
    }

    iterator end() {
        const int degree = this->degree();
        return (degree < 0)
            ? coefficients.begin() + 1
            : coefficients.begin() + degree + 1;

        return coefficients.begin() + degree + 1;
    }

    typedef typename std::deque<T>::const_iterator const_iterator;

    const_iterator begin() const {
        return coefficients.begin();
    }

    const_iterator end() const {
        const int degree = this->degree();
        return (degree < 0)
            ? coefficients.begin() + 1
            : coefficients.begin() + degree + 1;
    }

    const_iterator cbegin() const {
        return coefficients.cbegin();
    }

    const_iterator cend() const {
        const int degree = this->degree();
        return (degree < 0)
            ? coefficients.begin() + 1
            : coefficients.begin() + degree + 1;
    }

private:
    mutable std::deque<T> coefficients;
};


template <typename T>
std::ostream& operator<<(std::ostream& os, const Polynomial<T>& polynomial) {
    int degree = polynomial.degree();
    if (degree == -1) {
        os << "0";
        return os;
    }
    for (int i = polynomial.degree(); i > 0; --i) {
        if (polynomial[i] == 0) {
            continue;
        }
        if (polynomial[i] != 1) {
            os << polynomial[i];
        }
        os << "x";
        if (i > 1) {
            os << "^" << i;
        }
        os << " + ";
    }
    os << polynomial[0];
    return os;
}


template <typename T>
Polynomial<T> operator,(const Polynomial<T>& lhs, const Polynomial<T>& rhs) {
    Polynomial<T> a = (lhs.degree() > rhs.degree()) ? lhs : rhs;
    Polynomial<T> b = (lhs.degree() > rhs.degree()) ? rhs : lhs;
    Polynomial<T> remainder = b;
    Polynomial<T> gcd(0);

    while (remainder.degree() != -1) {
        gcd = remainder;
        Polynomial<T> quotient =  a / b;
        remainder = a % b;
        a = b;
        b = remainder;
    }
    return gcd.normalize();
};

template <typename T>
bool operator==(const T scalar, const Polynomial<T>& polynomial) {
    return polynomial == scalar;
}

template <typename T>
bool operator!=(const T scalar, const Polynomial<T>& polynomial) {
    return polynomial != scalar;
}

template <typename T>
Polynomial<T> operator+(const T scalar, const Polynomial<T>& polynomial) {
    Polynomial<T> result(polynomial);
    result += scalar;
    return result;
}

template <typename T>
Polynomial<T> operator-(const T scalar, const Polynomial<T>& polynomial) {
    Polynomial<T> result(-polynomial);
    result += scalar;
    return result;
}

template <typename T>
Polynomial<T> operator*(const T scalar, const Polynomial<T>& polynomial) {
    Polynomial<T> result(polynomial);
    result *= scalar;
    return result;
}

#endif //POLINOM_POLYNOM_H
